//nodejs v10.15.1, hapi 19 async/await

//npm install @hapi/hapi
const Hapi = require('@hapi/hapi');
//npm install sqlite https://github.com/kriasoft/node-sqlite
const sqlite = require('sqlite');
//npm install moment
var moment = require('moment');


//var dbfile = "/root/loctrack_2019.db";
var dbfile = "db/loctrack_2019.db";


const init = async () => {

    const server = Hapi.server({
        port: 8050,
        host: '0.0.0.0'
    });
    
async function get_rows_by_number(nombre){				
				var db = await sqlite.open(dbfile);
				var rows = await db.all("SELECT * from GEO ORDER BY id DESC LIMIT ?;", nombre);
				//console.log(rows);	
				return rows;				
}

server.route({
	    method: 'GET',
	    path:'/nlocs/{nblocs}', 
	    options:{ cors:true },
		handler: async (request, reply) => {
					return await get_rows_by_number(encodeURIComponent(request.params.nblocs));
			    },  
});

async function get_rows_by_date(date){	
	var epoch=moment(date).format('X');
	var db = await sqlite.open(dbfile);
	var rows = await db.all("SELECT * from GEO where recvtime > ?;", epoch);
	return rows;				
	
}

server.route({
            method: 'GET',
            path:'/by_day/{date}',
            options:{ cors:true },
            handler: async (request, reply) => {
					return await get_rows_by_date(request.params.date);
			    },
});


async function loctrack_log_dans_db(payload){		
				var ts = Math.floor(Date.now()/1000);
				//console.log('reception loctrack_log_dans_db %s, %s, %s, %s', lat, long, fixtime, ts);
				var db = await sqlite.open(dbfile);
				for(var oneloc of payload) {
					var rows = await db.all("INSERT into GEO(lat, long, fixtime, recvtime) VALUES((?),(?),(?),(?))", oneloc.lat, oneloc.long, oneloc.fixtime, ts);
				}
				return 0;				
}

//curl -d "lat=43.52192&long=3.92212&fixtime=1571487419" -X POST http://0.0.0.0:8050/post_loc
//curl --header "Content-Type: application/json" --request POST --data '[{"lat":"43.0","long":"3.0","fixtime":"1573289999"},{"lat":"43.5","long":"3.5","fixtime":"1573201234"}]' http://0.0.0.0:8050/post_loc
server.route({
            method: 'POST',
            path:'/post_loc',
            handler: async (request, reply) => {
					return await loctrack_log_dans_db(request.payload);
			    },
});





    
    

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();
