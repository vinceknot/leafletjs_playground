## 2020 sqlite -> json direct en shell:

sqlite3 -header -csv apps/vvnx.locgatt/db/loc.db "select id,fixtime,lat,long,acc from loc;" > locs.csv
mlr --icsv --ojson --jlistwrap cat locs.csv > data.json #ici "cat" est un "verb" chez mlr (miller)

## helloworld.html

locs au format [{lat: 42.7, lng: 1.3}, {lat: 42.7, lng: 1.5}, {lat: 42.8, lng: 1.4}];

## geojson.html 

locs en geojson

## analyseDeSet.html

un array de locs locales (data/XXXX.js) importées dans une layer leaflet en itérant et en créant à chaque item un 
L.circleMarker, appendé à une layerGroup. A chaque itération les options (color) du circleMarker sont conditionnellement déterminées
pour analyse: exple: recvtime - fixtime.
